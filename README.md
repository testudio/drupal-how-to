# Drupal How-To

This is a blog that provides detailed and practical advice about the way to do something in Drupal (a how-to).
## How-to

- [Implement auto-incremeting label](/How-tos/implement-auto-incremeting-label.md)
- [Edit entity within a modal dialog in a view](/How-tos/edit-entity-within-dialog.md)

## Usage
Use any how-to at your own risk.

## Contributing
If you have a how-to question - create an issue in the inssue queue.

## Authors and acknowledgment
Tomato Elephant Studio employees 

## License
CC0 1.0 Universal - CC0 public domain
