# How-To: Implement auto-incremeting label.

**Scenario**: Implement auto-incremeting label on a content type "Invoice", example "Invoice #1001". 
Each content type should have it's own auto-incrementing number (in case "Quote" content type will be introduced). 

- Platform: Drupal 10
- Coding: none
- Prerequisites: [Token](https://www.drupal.org/project/token) 

## Install and configure modules

1. [Serial Field](https://www.drupal.org/project/serial) 

This module provides an auto-increment (serial) field per content type. 

It's a pretty popular module with **7,384 sites reporting using this module** and actively maintained.
Created by kirsh on 2 October 2009.

```
composer require 'drupal/serial:^2.0@beta'
./vendor/bin/drush pm:en serial

```

After enabling module:
- Add a Serial field to a content type  
![Add a Serial field to a content type](/How-tos/images/implement-auto-incremeting-label/serial-field-add.png "Serial field")
- Configure serial field, for example set starting value to `1000`.
![Add a Serial field to a content type](/How-tos/images/implement-auto-incremeting-label/serial-field-configure.png "Serial field")
- No updates to entity form is required (this field is "invisible")
- Hide this field on entity display 
![Add a Serial field to a content type](/How-tos/images/implement-auto-incremeting-label/content-type-display-config.png "Serial field")

2. [Automatic Entity Label](https://www.drupal.org/project/auto_entitylabel) 

"Automatic Entity Label" is a small and efficient module that sets label automatically by a given pattern.

It's a pretty popular module with **44,898 sites reporting using this module** and actively maintained. 
Created by bforchhammer on 6 February 2012.

```
composer require 'drupal/auto_entitylabel:^3.0'
./vendor/bin/drush pm:en auto_entitylabel
```

After enabling module:
- Navigate to content type's management page: `/admin/structure/types/manage/invoice/auto-label`
- Set auto label option to `Automatically generate the label and hide the label field`
- Set pattern to `Invoice #[node:field_serial_no]`

![Congiruation of auto entity label](/How-tos/images/implement-auto-incremeting-label/auto-entity-label-config.png "Configuration")

Note: There are extra options, eg update any existing nodes' titles

## Test the solution

Create a new node of type Invoice - and check it's title!

Here's an example of the view of Invoices with title and serial number:

![Example view of nodes with titles](/How-tos/images/implement-auto-incremeting-label/invoices-view.png "View of entities with auto-incrementing labels")


## Gotchas
- When creating a new node - there's no "preview" of the serial number as it hasn't been generated yet
- Title text field is hidden on the entity form, it would be nicer if it was just disabled (patch anyone?)
- If you're using "Replicate" module to copy a node, the label will get set to the configured pattern (not "Copy of XXX" by Replicate module).




