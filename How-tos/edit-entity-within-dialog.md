# How To: Edit entity within a modal dialog window.

Scenario: Simplify user interface for editors on long lists of entities - open edit windown without full page reload for some simple entities (eg edit taxonomy terms).

- Platform: Drupal 10
- Coding: minimal Twig/HTML

## How To

- Create a view of the entities 
- Display fields (label/name, description, link to edit)
- Add a Custom Text field with the following code (this is for a taxonomy terms view)
- Test it!
![Edit in dialog box](/How-tos/images/edit-entity-dialog/view-edit-term.png "Dialog edit")
```

{% if edit_taxonomy_term is not empty %}
<a class="use-ajax" 
    data-dialog-options="{&quot;width&quot;:800}" 
    data-dialog-type="modal" 
    href="{{ edit_taxonomy_term }}?destination={{ path('current') }}">{{ 'Edit'|t }}</a>
{% elseif %}

```

## Gotchas
- unfortunately {{ path('current') }} doesn't return full query string
